{ 
  "abstract" : "<jats:p>Abstract. The little-known Awu volcano (Sangihe Islands, Indonesia) is among the\ndeadliest, with a cumulative death toll of 11 048. In less than 4 centuries, 18 eruptions were recorded, including two VEI 4 and three VEI 3 eruptions with worldwide impacts. The regional geodynamic setting is controlled by a divergent-double-subduction collision and an arc–arc collision. In that context, the slab stalls in the mantle, undergoes an increase in temperature, and becomes prone to melting, a process that sustained the magmatic supply. Awu also has the particularity of hosting alternatively and simultaneously a lava dome and a crater lake throughout its activity. The lava dome passively erupted through the crater lake and induced strong water evaporation from the crater. A conduit plug associated with this dome emplacement subsequently channeled the gas emission to the crater wall. However, with the lava dome cooling, the high annual rainfall eventually reconstituted the crater lake and created a hazardous situation on Awu. Indeed with a new magma injection, rapid pressure buildup may pulverize the conduit plug and the lava dome, allowing lake water injection and subsequent explosive water–magma interaction. The past vigorous eruptions are likely induced by these phenomena, possible scenarios for future events.\n                    </jats:p>",
  "author" : [ 
      { 
        "affiliation" : [  ],
        "family" : "Bani",
        "given" : "Philipson",
        "sequence" : "first"
      },
      { 
        "affiliation" : [  ],
        "family" : "Kunrat",
        "given" : "Syegi",
        "sequence" : "additional"
      },
      { 
        "affiliation" : [  ],
        "family" : "Syahbana",
        "given" : "Devy Kamil",
        "sequence" : "additional"
      },
      { 
        "affiliation" : [  ],
        "name" : "Kristianto",
        "sequence" : "additional"
      }
    ],
  "container-title" : "Natural Hazards and Earth System Sciences",
  "container-title-short" : "Nat. Hazards Earth Syst. Sci.",
  "content-domain" : { 
      "crossmark-restriction" : 0,
      "domain" : [  ]
    },
  "created" : { 
      "date-parts" : [ [ 
            2020,
            8,
            8
          ] ],
      "date-time" : "2020-08-08T03:23:06Z",
      "timestamp" : 1596856986000
    },
  "deposited" : { 
      "date-parts" : [ [ 
            2020,
            9,
            14
          ] ],
      "date-time" : "2020-09-14T18:29:57Z",
      "timestamp" : 1600108197000
    },
  "DOI" : "10.5194/nhess-20-2119-2020",
  "indexed" : { 
      "date-parts" : [ [ 
            2020,
            11,
            21
          ] ],
      "date-time" : "2020-11-21T15:24:26Z",
      "timestamp" : 1605972266851
    },
  "is-referenced-by-count" : 1,
  "ISSN" : [ "1684-9981" ],
  "issue" : "8",
  "issued" : { "date-parts" : [ [ 
            2020,
            8,
            7
          ] ] },
  "journal-issue" : { 
      "issue" : "8",
      "published-online" : { "date-parts" : [ [ 2020 ] ] }
    },
  "language" : "en",
  "license" : [ { 
        "content-version" : "unspecified",
        "delay-in-days" : 0,
        "start" : { 
            "date-parts" : [ [ 
                  2020,
                  8,
                  7
                ] ],
            "date-time" : "2020-08-07T00:00:00Z",
            "timestamp" : 1596758400000
          },
        "URL" : "https://creativecommons.org/licenses/by/4.0/"
      } ],
  "link" : [ { 
        "content-type" : "unspecified",
        "content-version" : "vor",
        "intended-application" : "similarity-checking",
        "URL" : "https://nhess.copernicus.org/articles/20/2119/2020/nhess-20-2119-2020.pdf"
      } ],
  "member" : "3145",
  "original-title" : [  ],
  "page" : "2119-2132",
  "prefix" : "10.5194",
  "published-online" : { "date-parts" : [ [ 
            2020,
            8,
            7
          ] ] },
  "publisher" : "Copernicus GmbH",
  "reference" : [ 
      { 
        "DOI" : "10.1029/2011JB008461",
        "doi-asserted-by" : "crossref",
        "key" : "ref1",
        "unstructured" : "Aiuppa, A., Shinohara, H., Tamburello, G., Giudice, G., Liuzzo, M., and Moretti, R.: Hydrogen in the gas plume of an open-vent volcano, Mount Etna, Italy, J. Geophys. Res.-Solid, 116, B10204, https://doi.org/10.1029/2011JB008461, 2011."
      },
      { 
        "DOI" : "10.1016/j.jvolgeores.2015.09.008",
        "doi-asserted-by" : "crossref",
        "key" : "ref2",
        "unstructured" : "Aiuppa, A., Bani, P., Moussallam, Y., Di Napoli, R., Allard, P., Gunawan,\nH., Hendrasto, M., and Tamburello, G.: First determination of magmaderived gas emissions from Bromo volcano, eastern Java (Indonesia), J. Volcanol. Geoth. Res., 304, 206–213, https://doi.org/10.1016/j.jvolgeores.2015.09.008, 2015."
      },
      { 
        "DOI" : "10.1130/G45065.1",
        "doi-asserted-by" : "crossref",
        "key" : "ref3",
        "unstructured" : "Aravena, A., de' Michieli Vitturi, M., Cioni, R., and Neri, A.: Physical\nconstraints for effective magma-water interaction along volcanic conduits\nduring silicic explosive eruptions, Geology, 46, 867–870,\nhttps://doi.org/10.1130/G45065.1, 2018."
      },
      { 
        "DOI" : "10.1002/2014JB011519",
        "doi-asserted-by" : "crossref",
        "key" : "ref4",
        "unstructured" : "Ashwell, P. A., Kendrick, J. E., Lavallée, Y., Kennedy, B. M., Hess,\nK.-U., von Aulock, F. W., Wadsworth, F. B., Vasseur, J., and Dingwell, D. B.:\nPermeability of compacting porous lavas, J. Geophys. Res.-Solid, 120,\n1605–1622, https://doi.org/10.1002/2014JB011519, 2015."
      },
      { 
        "DOI" : "10.5194/nhess-9-1471-2009",
        "doi-asserted-by" : "crossref",
        "key" : "ref5",
        "unstructured" : "Bani, P., Join, J.-L., Cronin, S. J., Lardy, M., Rouet, I., and Garaebiti, E.: Characteristics of the summit lakes of Ambae volcano and their potential for generating lahars, Nat. Hazards Earth Syst. Sci., 9, 1471–1478, https://doi.org/10.5194/nhess-9-1471-2009, 2009."
      },
      { 
        "DOI" : "10.1007/s00445-017-1142-8",
        "doi-asserted-by" : "crossref",
        "key" : "ref6",
        "unstructured" : "Bani, P., Alfianti, H., Aiuppa, A., Oppenheimer, C., Sitinjak, P., Tsanev, V., and Saing, U. B.: First study of heat and gas budget for Sirung volcano,\nIndonesia, B. Volcanol., 79, 60, https://doi.org/10.1007/s00445-017-1142-8, 2017."
      },
      { 
        "DOI" : "10.1007/s00445-017-1178-9",
        "doi-asserted-by" : "crossref",
        "key" : "ref7",
        "unstructured" : "Bani, P., Tamburello, G., Rose-Koga, E. F., Liuzzo M., Aiuppa, A., Cluzel,\nN., Amat, I., Syahbana, D. K., and Gunawan, H.: Dukono, the predominant source of volcanic degassing in Indonesia, sustained by a depleted Indian-MORB, B. Volcanol., 80, 5, https://doi.org/10.1007/s00445-017-1178-9, 2018."
      },
      { 
        "key" : "ref8",
        "unstructured" : "Bani, P., Le Glas, E., Kristianto, Aiuppa, A., and Syahbana, D. K.: An elevated CO2 gas content at Awu volcano, Sangihe arc, Indonesia Geosci. Lett., submitted, 2020."
      },
      { 
        "DOI" : "10.1038/nature09799",
        "doi-asserted-by" : "crossref",
        "key" : "ref9",
        "unstructured" : "Burgisser, A. and Bergantz, G. W. A.: Rapid mechanism to remobilize and\nhomogenize highly crystalline magma bodies, Nature, 471, 212–215, 2011."
      },
      { 
        "DOI" : "10.1029/GM023p0001",
        "doi-asserted-by" : "crossref",
        "key" : "ref10",
        "unstructured" : "Cardwell, R. K., Isacks, B. L., and Karig, D. E.: The spatial distribution of\nearthquakes, focal mechanism solutions and subducted lithosphere in the\nPhilippine and northeast Indonesian islands, Am. Geophys. Union, Geophys. Monogr., 23, 1–36, 1980."
      },
      { 
        "key" : "ref11",
        "unstructured" : "Cashman, K. V.: Crystallization of Mount St. Helens 1980–1986 dacite: a\nquantitative textural approach, B. Volcanol., 50, 194–209, 1988."
      },
      { 
        "key" : "ref12",
        "unstructured" : "Cashman, K. V.: Groundmass crystallization of Mount St. Helens dacite,\n1980–1986: a tool for interpreting shallow magmatic processes, Contrib. Mineral. Petrol., 109, 431–449, 1992."
      },
      { 
        "DOI" : "10.1029/2018GC008161",
        "doi-asserted-by" : "crossref",
        "key" : "ref13",
        "unstructured" : "Cassidy, M., Ebmeier, S. K., Helo, C., Watt, S. F. L., Caudron, C., Odell, A., Spaans, K., Kristianto, P., Tristuty, H., Gunawan, H., and Castro, J. M.:\nExplosive eruptions with little warning: Experimental petrology and volcano\nmonitoring observations from the 2014 eruption of Kelud, Indonesia,\nGeochem. Geophy. Geosy., 20, 4218–4247, https://doi.org/10.1029/2018GC008161, 2019."
      },
      { 
        "DOI" : "10.1002/2015GL064885",
        "doi-asserted-by" : "crossref",
        "key" : "ref14",
        "unstructured" : "Caudron, C., Taisne, B., Garcés, M., Alexis, L. P., and Mialle, P.: On the use of remote infrasound and seismic stations to constrain the eruptive\nsequence and intensity for the 2014 Kelud eruption, Geophys. Res. Lett., 42,\n614–621, https://doi.org/10.1002/2015GL064885, 2015."
      },
      { 
        "DOI" : "10.1029/2004GC000825",
        "doi-asserted-by" : "crossref",
        "key" : "ref15",
        "unstructured" : "Clor, L. E., Fischer, T. P., Hilton, D. R., Sharp, Z. D., and Hartono, U.: Volatile and N isotope chemistry of the Molucca Sea collision zone: Tracing source components along the Sangihe Arc, Indonesia, Geochem. Geophy. Geosy., 6, Q03J14, https://doi.org/10.1029/2004GC000825, 2005."
      },
      { 
        "DOI" : "10.1016/j.jvolgeores.2012.12.025",
        "doi-asserted-by" : "crossref",
        "key" : "ref16",
        "unstructured" : "Costa, F., Andreastuti, S., Bouvet de Maisonneuve, C., and Pallister, J. S.:\nPetrological insights into the storage conditions, and magmatic processes\nthat yielded the centennial 2010 Merapi explosive eruption, J. Volcanol. Geoth. Res., 261, 209–235, 2013."
      },
      { 
        "key" : "ref17",
        "unstructured" : "Data Dasar Gunung Api, Wilaya Timur: edisi kedua. Kementerian Energi dan\nSumber Daya Mineral, Badan Geologi, 1–450, 2011."
      },
      { 
        "DOI" : "10.1029/GM096p0251",
        "doi-asserted-by" : "crossref",
        "key" : "ref18",
        "unstructured" : "Davidson, J. P.: Deciphering mantle and crustal signatures in subduction\nzones, in: Subduction: top to bottom, edited by: Bebout, G. E., Scholl, D. W., Kirby, S. H., and Platt, J. P., Am. Geophys. Union Monogr., 96, 251–262,\n1996."
      },
      { 
        "key" : "ref19",
        "unstructured" : "Delfin, F. G., Newhall, C. G., Martinez, M. L., Salonga, N. D., Bayon, F. E.\nB., Trimble, D., and Solidum, R.: Geological, 14C and\nhistoricalevidence for a 17th century eruption of Parker volcano, Mindanao,\nPhilippines, J. Geol. Soc. Philipp., 52, 25–42, 1997."
      },
      { 
        "DOI" : "10.1029/2002GL014858",
        "doi-asserted-by" : "crossref",
        "key" : "ref20",
        "unstructured" : "Donarummo Jr., J., Ram, M., and Stolz, M. R.: Sun/dust correlations and volcanic interference, Geophys. Res. Lett., 29, 1361, https://doi.org/10.1029/2002GL014858, 2002."
      },
      { 
        "DOI" : "10.1002/wea.2786",
        "doi-asserted-by" : "crossref",
        "key" : "ref21",
        "unstructured" : "Fikke, S. M., Kristjánsson, J. E., and Nordli, O.: Screaming clouds,\nWeather, 72, 115–121, https://doi.org/10.1002/wea.2786, 2017."
      },
      { 
        "DOI" : "10.1029/92JB00416",
        "doi-asserted-by" : "crossref",
        "key" : "ref22",
        "unstructured" : "Fink, J. H., Anderson, S. W., and Manley, C. R.: Textural constraints on effusive silicic volcanism: Beyond the permeable foam model, J. Geophys. Res., 97, 9073–9083, https://doi.org/10.1029/92JB00416, 1992."
      },
      { 
        "DOI" : "10.1016/j.jvolgeores.2012.05.015",
        "doi-asserted-by" : "crossref",
        "key" : "ref23",
        "unstructured" : "Gudmundsson, A.: Magma chambers: Formation, local stresses, excess pressures, and compartments, J. Volcanol. Geoth. Res., 237–238, 19–41, 2012."
      },
      { 
        "DOI" : "10.1002/2015GL066154",
        "doi-asserted-by" : "crossref",
        "key" : "ref24",
        "unstructured" : "Guevara-Murua, A., Hendy, E. J., Rust, A. C., and Cashman, K. V.: Consistent\ndecrease in North Atlantic Tropical Cyclone frequency following major volcanic eruption in the last three centuries, Geophys. Res. Lett., 42,\n9425–9432, https://doi.org/10.1002/2015GL066154, 2015."
      },
      { 
        "DOI" : "10.5479/si.GVP.BGVN199202-267040",
        "doi-asserted-by" : "crossref",
        "key" : "ref25",
        "unstructured" : "GVP – Global Volcanism Program: Report on Awu (Indonesia), edited by: McClelland, L., Bulletin of the Global Volcanism Network, Smithsonian\nInstitution, 17&amp;thinsp;pp., https://doi.org/10.5479/si.GVP.BGVN199202-267040, 1992."
      },
      { 
        "DOI" : "10.5479/si.GVP.BGVN200405-267040",
        "doi-asserted-by" : "crossref",
        "key" : "ref26",
        "unstructured" : "GVP – Global Volcanism Program: Report on Awu (Indonesia), edited by: Wunderman, R., Bulletin of the Global Volcanism Network, Smithsonian Institution, 29&amp;thinsp;pp., https://doi.org/10.5479/si.GVP.BGVN200405-267040, 2004."
      },
      { 
        "key" : "ref27",
        "unstructured" : "GVP – Global Volcanism Program: Awu (267040) in: Volcanoes of the World, v. 4.8.8, editied by: Venzke, E., Smithsonian Institution, available at:\nhttps://volcano.si.edu/volcano.cfm?vn=267040 (last access: 5 August 2020), 2013a."
      },
      { 
        "DOI" : "10.5479/si.GVP.VOTW4-2013",
        "doi-asserted-by" : "crossref",
        "key" : "ref28",
        "unstructured" : "GVP – Global Volcanism Program: Awu (267040), in: Volcanoes of the World, v. 4.8.5, edited by: Venzke, E., Smithsonian Institution,\nhttps://doi.org/10.5479/si.GVP.VOTW4-2013, 2013b."
      },
      { 
        "DOI" : "10.1016/S1367-9120(00)00040-7",
        "doi-asserted-by" : "crossref",
        "key" : "ref29",
        "unstructured" : "Hall, R. and Wilson, M. E. J.: Neogene sutures in eastern Indonesia, J. Asian Earth Sci., 18, 781–808, 2000."
      },
      { 
        "DOI" : "10.1016/0040-1951(95)00038-0",
        "doi-asserted-by" : "crossref",
        "key" : "ref30",
        "unstructured" : "Hall, R., Ali, J. R., Anderson, C. D., and Baker, S. J.: Origin and motion history of the Philippine Sea Plate, Tectonophysics, 251, 229–250, 1995."
      },
      { 
        "DOI" : "10.1029/GL011i011p01121",
        "doi-asserted-by" : "crossref",
        "key" : "ref31",
        "unstructured" : "Handler, P.: Possible association of stratospheric aerosols and El Nino type\nevents, Geophys. Res. Lett., 11, 1121–1124, 1984."
      },
      { 
        "DOI" : "10.1029/2012GC004346",
        "doi-asserted-by" : "crossref",
        "key" : "ref32",
        "unstructured" : "Hanyu, T., Gill, J., Tatsumi, Y., Kimura, J.-I., Sato, K., Chang, Q., Senda, R., Miyazaki, T., Hirahara, Y., Takahashi, T., and Zulkarnain, I.: Across- and along-arc geochemical variations of lava chemistry in the Sangihe arc: Various fluid and melt slab fluxes in response to slab temperature, Geochem. Geophy. Geosy., 13, Q10021, https://doi.org/10.1029/2012GC004346, 2012."
      },
      { 
        "DOI" : "10.1017/CBO9781139029346",
        "doi-asserted-by" : "crossref",
        "key" : "ref33",
        "unstructured" : "Harris, A.: Thermal Remote Sensing of Active Volcanoes, A user's Manual,\nCambridge University Press, Cambridge, UK, 1–728, 2013."
      },
      { 
        "DOI" : "10.1038/s41467-019-13102-8",
        "doi-asserted-by" : "crossref",
        "key" : "ref34",
        "unstructured" : "Heap, M. J., Troll, V. R., Kushnir, A. R. L., Gilg, H. A., Collinson, A. S. D., Deegan, F. M., Darmawan, H., Seraphine, N., Neuberg, J., and Walter, T. R.: Hydrothermal alteration of andesitic lava domes can lead to explosive\nvolcanic behaviour, Nat. Commun., 10, 5063, https://doi.org/10.1038/s41467-019-13102-8, 2019."
      },
      { 
        "DOI" : "10.17014/ijog.v4i4.83",
        "doi-asserted-by" : "crossref",
        "key" : "ref35",
        "unstructured" : "Hidayati, S., Basuki, A., Kristianto, and Mulyana, I.: Emergence of Lava Dome\nfrom the Crater Lake of Kelud Volcano, East Java, J. Geologi. Indonesia, 4,\n229–238, 2009."
      },
      { 
        "DOI" : "10.1029/2003GC000660",
        "doi-asserted-by" : "crossref",
        "key" : "ref36",
        "unstructured" : "Jaffe, L. A., Hilton, D. R., Fisher, T. P., and Hartono, U.: Tracing magma\nsources in an arc-arc colliding zone: Helium and carbon isotope and relative\nabundance systematic of the Sangihe Arc, Indonesia, Geochem. Geophy. Geosy., 5, Q04J10, https://doi.org/10.1029/2003GC000660, 2004."
      },
      { 
        "DOI" : "10.1029/94GL03113",
        "doi-asserted-by" : "crossref",
        "key" : "ref37",
        "unstructured" : "Jones, P. D., Briffa, K. R., and Schweingruber, F. H.: Tree-ring evidence of the widespread effects of explosive volcanic eruption, Geophys. Res. Lett., 22, 1333–1336, 1995."
      },
      { 
        "DOI" : "10.1007/s004450050128",
        "doi-asserted-by" : "crossref",
        "key" : "ref38",
        "unstructured" : "Klug, C. and Cashman, K. V.: Permeability development in vesiculating magmas:\nImplications for fragmentation, B. Volcanol., 58, 87–100, 1996."
      },
      { 
        "DOI" : "10.1002/2014GL062307",
        "doi-asserted-by" : "crossref",
        "key" : "ref39",
        "unstructured" : "Kristiansen, N. I., Prata, A. J., Stohl, A., and Carn, S. A.: Stratospheric\nvolcanic ash emissions from the 13 February 2014 Kelut eruption, Geophys.\nRes. Lett., 42, 588–596, https://doi.org/10.1002/2014GL062307, 2015."
      },
      { 
        "DOI" : "10.1007/s00445-006-0110-5",
        "doi-asserted-by" : "crossref",
        "key" : "ref40",
        "unstructured" : "Lagmay, A. M. F., Rodolfo, K. S., Siringan, F. P., Uy, H., Remotigue, C.,\nZamora, P., Lapus, M., Rodolfo, R., and Ong, J.: Geology and hazard implications of the Maraunot notch in the Pinatubo Caldera, Philippines, B. Volcanol., 69, 797–809, 2007."
      },
      { 
        "DOI" : "10.1007/BF02600578",
        "doi-asserted-by" : "crossref",
        "key" : "ref41",
        "unstructured" : "Latter, J. H.: Tsunamis of Volcanic Origin: Summary of Causes, with\nParticular Reference to Krakatoa, 1883, B. Volcanol., 44, 467–490, 1981."
      },
      { 
        "DOI" : "10.1144/GSL.SP.2003.219.01.10",
        "doi-asserted-by" : "crossref",
        "key" : "ref42",
        "unstructured" : "Macpherson, C. G., Forde, E. J., Hall, R., and Thirlwall, M. F.: Geochemical\nevolution of magmatism in an arc-arc collision: the Halmahera and Sangihe\narcs, eastern Indonesia, in: Intraoceanic Subduction systems: tectonic and magmatic processes, vol. 219, edited by: Larter, R. D. and Leat, P. T., Special Publications, Geological Society, London, 207–220,\nhttps://doi.org/10.1144/GSL.SP.2003.219.01.10, 2003."
      },
      { 
        "DOI" : "10.1007/978-3-642-36833-2_2",
        "doi-asserted-by" : "crossref",
        "key" : "ref43",
        "unstructured" : "Manville, V.: Volcano-Hydrologic hazards from Volcanic Lakes, in: Volcanic Lakes, Advances in Volcanology, edited by: Rouet, D., Christensen, B., Tassi, F., and Vandemeulebrouck, J., Springer-Verlag, Berlin, Heidelberg, https://doi.org/10.1007/978-3-642-36833-2_2, 2015."
      },
      { 
        "DOI" : "10.1007/s004450050176",
        "doi-asserted-by" : "crossref",
        "key" : "ref44",
        "unstructured" : "Matthews, S. J., Gardeweg, M. C., and Sparks, R. S. J.: The 1984 to 1996 cyclic activity of Lascar Volcano, northern Chile: cycles of dome growth, dome subsidence, degassing and explosive eruptions, B. Volcanol., 59, 72–82, 1997."
      },
      { 
        "DOI" : "10.1016/0040-1951(83)90243-3",
        "doi-asserted-by" : "crossref",
        "key" : "ref45",
        "unstructured" : "McCaffrey, R.: Seismic-wave propagation beneath the Molucca Sea arc-arc\ncollision zone, Indonesia, Tectonophysics, 96, 45–57,\nhttps://doi.org/10.1016/0040-1951(83)90243-3, 1983."
      },
      { 
        "DOI" : "10.1016/0012-821X(91)90029-H",
        "doi-asserted-by" : "crossref",
        "key" : "ref46",
        "unstructured" : "McCulloch, M. T. and Gamble, J. A.: Geochemical and geodynamical constraints on subduction zone magmatims, Earth Planet. Sc. Lett., 102, 358–374.\nhttps://doi.org/10.1016/0012-821X(91)90029-H, 1991."
      },
      { 
        "DOI" : "10.1029/2004JB003183",
        "doi-asserted-by" : "crossref",
        "key" : "ref47",
        "unstructured" : "Melnik, O. and Sparks, R. S. J.: Controls on conduit magma flow dynamics during lava dome building eruptions, J. Geophys. Res., 110, B02209,\nhttps://doi.org/10.1029/2004JB003183, 2005."
      },
      { 
        "DOI" : "10.1016/0377-0273(83)90129-4",
        "doi-asserted-by" : "crossref",
        "key" : "ref48",
        "unstructured" : "Morrice, M. G., Jezek, P. A., Gill, J. B., Withford, D. J., and Monoarfa, M.: An introduction to the Sangihe arc: volcanism accompanying arc-arc collision in the Molucca sea, Indonesia, J. Volcanol. Geoth. Res., 19, 135–165, 1983."
      },
      { 
        "DOI" : "10.1007/s00445-017-1129-5",
        "doi-asserted-by" : "crossref",
        "key" : "ref49",
        "unstructured" : "Moussallam, Y., Peters, N., Masias, P., Aaza, F., Barnie, T., Schipper, C.\nI., Curtis, A., Tamburello, G., Aiuppa, A., Bani, P., Giudice, G., Pieri,\nD., Davies, A. G., and Oppenheimer, C.: Magmatic gas percolation through the old lava dome of El Misti volcano, B. Volcanol., 79, 46, https://doi.org/10.1007/s00445-017-1129-5, 2017."
      },
      { 
        "DOI" : "10.1130/G24605A.1",
        "doi-asserted-by" : "crossref",
        "key" : "ref50",
        "unstructured" : "Mueller, S., Scheu, B., Spieler, O., and Dingwell, D. B.: Permeability control on magma fragmentation, Geology, 36, 399–402, 2008."
      },
      { 
        "DOI" : "10.1016/0377-0273(83)90064-1",
        "doi-asserted-by" : "crossref",
        "key" : "ref51",
        "unstructured" : "Newhall, C. G. and Melson, W. G.: Explosive activity associated with the\ngrowth of volcanic domes, J. Volcanol. Geoth. Res., 17, 111–131, 1983."
      },
      { 
        "DOI" : "10.1029/JC087iC02p01231",
        "doi-asserted-by" : "crossref",
        "key" : "ref52",
        "unstructured" : "Newhall, C. G. and Self, S.: The Volcanic Explosivity Index (VEI): An Estimate of Explosivity Magnitude for Historical Volcanism, J. Geophys. Res., 87, 1231–1238, 1982."
      },
      { 
        "key" : "ref53",
        "unstructured" : "Olson, D. W., Doescher, R. L., and Olson, M. S.: When the sky ran red: The story behind “the Scream”, Sky Telescope, 107, 28–35, 2004."
      },
      { 
        "DOI" : "10.1038/356426a0",
        "doi-asserted-by" : "crossref",
        "key" : "ref54",
        "unstructured" : "Pallister, J. S., Hoblitt, R. P., and Reyes, A. G. A.: Basalt trigger for the 1991 eruptions of Pinatubo volcano, Nature, 356, 426–428, 1992."
      },
      { 
        "DOI" : "10.1029/2001JD000330",
        "doi-asserted-by" : "crossref",
        "key" : "ref55",
        "unstructured" : "Palmer, A. S., van Ommen, T. D., Curran, M. A. J., Morgan, V., Souney, J.\nM., and Mayewski, P. A.: High-precision dating of volcanic events (A.D. 1301–1995) using ice cores from Law Dom, Antarctica, J. Geophys. Res., 106, 28089–28095, 2001."
      },
      { 
        "DOI" : "10.1007/s11069-013-0822-8",
        "doi-asserted-by" : "crossref",
        "key" : "ref56",
        "unstructured" : "Paris, R., Switzer, A. D., Belousova, M., Belousov, A., Ontowirjo, B., Whelley, P. L., and Ulvrova, M.: Volcanic tsunami: a review of source\nmechanisms, past events and hazards in Southeast Asia (Indonesia, Philippines, Papua New Guinea), Nat. Hazards, 70, 447–470,\nhttps://doi.org/10.1007/s11069-013-0822-8, 2014."
      },
      { 
        "DOI" : "10.1038/nature17401",
        "doi-asserted-by" : "crossref",
        "key" : "ref57",
        "unstructured" : "Parmigiani, A., Faroughi, S., Huber, C., Bachmann, O., and Su, Y.: Bubble\naccumulation and its role in the evolution of magma reservoirs in the upper\ncrust, Nature, 532, 492–495, 2016."
      },
      { 
        "DOI" : "10.1175/BAMS-D-17-0144.1",
        "doi-asserted-by" : "crossref",
        "key" : "ref58",
        "unstructured" : "Prata, F., Robock, A., and Hamblyn, R.: The Sky in Edvard Munch's The Scream,\nB. Am. Meteorol. Soc., 99, 1377–1390, https://doi.org/10.1175/BAMS-D-17-0144.1, 2018."
      },
      { 
        "DOI" : "10.1007/s00445-017-1104-1",
        "doi-asserted-by" : "crossref",
        "key" : "ref59",
        "unstructured" : "Primulyana, S., Bani, P., and Harris, A.: The effusive-explosive transitions at Rokatenda 2012–2013; unloading by extrusion of degassed magma with lateral gas flow, B. Volcanol., 79, 22, https://doi.org/10.1007/s00445-017-1104-1, 2017."
      },
      { 
        "DOI" : "10.1016/0743-9547(91)90070-E",
        "doi-asserted-by" : "crossref",
        "key" : "ref60",
        "unstructured" : "Pubellier, M., Quebral, R., Rangin, C., Deffontaines, B., Muller, C., and Manzano, J.: The Mindanao collision zone: a soft collision event within a continuous Neogen strike-slip setting, J. Southeast Asian Earth Sci., 6, 239–248, 1991."
      },
      { 
        "DOI" : "10.1016/B978-0-12-385938-9.00013-4",
        "doi-asserted-by" : "crossref",
        "key" : "ref61",
        "unstructured" : "Pyle, D. M.: Size of Volcanic Eruptions, in: The Encyclopedia of Volcanoes\n2nd Edn., edited by: Sigurdsson, H., Houghton, B., McNutt, S. R., Rymer, H., and Stix, J., Academic Press, Elsevier, London, UK, 257–264, https://doi.org/10.1016/B978-0-12-385938-9.00013-4, 2015."
      },
      { 
        "DOI" : "10.1016/0377-0273(81)90076-7",
        "doi-asserted-by" : "crossref",
        "key" : "ref62",
        "unstructured" : "Robock, A.: A latitudinally dependent volcanic dust veil index, and its effect on climaye simulations, J. Volcanol. Geoth. Res., 11, 67–80, 1981."
      },
      { 
        "DOI" : "10.1029/1998RG000054",
        "doi-asserted-by" : "crossref",
        "key" : "ref63",
        "unstructured" : "Robock, A.: Volcanic eruptions and climate, Rev. Geophys., 38, 191–219, 2000."
      },
      { 
        "DOI" : "10.1016/B978-0-12-385938-9.00053-5",
        "doi-asserted-by" : "crossref",
        "key" : "ref64",
        "unstructured" : "Robock, A.: Climatic Impacts of Volcanic Eruptions, in: The Encyclopedia of\nVolcanoes, 2nd Edn., edited by: Sigurdsson, H., Houghton, B., McNutt, S. R., Rymer, H., and Stix, J., Academic Press, Elsevier, London, UK, 935–942, https://doi.org/10.1016/B978-0-12-385938-9.00053-5, 2015."
      },
      { 
        "DOI" : "10.1016/0377-0273(83)90060-4",
        "doi-asserted-by" : "crossref",
        "key" : "ref65",
        "unstructured" : "Sheridan, M. F. and Wohletz, K. H.: Hydrovotcanism: basic considerations and\nreview, J. Volcanol. Geoth. Res., 17, 1–29, 1983."
      },
      { 
        "DOI" : "10.3133/pp1750",
        "doi-asserted-by" : "crossref",
        "key" : "ref66",
        "unstructured" : "Sherrod, D. R., Scott, W. E., and Stauffer, P. H.: A volcano Rekindled: The\nRenewed Eruption of Mount St. Helens, 2004–2006, US Geological Survey,\nProfessionnal Paper 1750, US Geological Survey, p. 856, 2008."
      },
      { 
        "DOI" : "10.1525/9780520947931",
        "doi-asserted-by" : "crossref",
        "key" : "ref67",
        "unstructured" : "Siebert, L., Simkin, T., and Kimberly, P.: Volcanoes of the World, 3rd Edn., University of California Press Books, Oakland, CA, 1–155, 2010."
      },
      { 
        "key" : "ref68",
        "unstructured" : "Simkin, T. P., Siebert, L., McCelland, L., Bridge, D., Newhall, C., and Latter, J. H.: Volcanoes of the World, Hutchinson-Ross, Stroudsberg, 1981."
      },
      { 
        "DOI" : "10.1038/267315a0",
        "doi-asserted-by" : "crossref",
        "key" : "ref69",
        "unstructured" : "Sparks, S. R. J., Sigurdsson, H., and Wilson, L.: Magma mixing: a mechanism for triggering acid explosive eruptions, Nature, 267, 315–318, 1977."
      },
      { 
        "DOI" : "10.1144/GSL.SP.2003.213.01.02",
        "doi-asserted-by" : "crossref",
        "key" : "ref70",
        "unstructured" : "Sparks, R. S. J.: Dynamics of magma degassing, in: Volcanic Degassing, edited by: Oppenheimer, C., Pyle, D. M., and Barclay, J., Geol. Soc. Spec. Publ.,\n213, 5–22, 2003."
      },
      { 
        "key" : "ref71",
        "unstructured" : "Stone, M.: Sangihe Property, Sangihe Island, North Sulawesi, Indonesia, Independent Technical Report, p. 130, 2020."
      },
      { 
        "DOI" : "10.1007/BF01067953",
        "doi-asserted-by" : "crossref",
        "key" : "ref72",
        "unstructured" : "Swanson, S. E., Naney, M. T., Wetrich, H. R., and Eichelberger, J. C.:\nCrystallization history of Obsidian dome, Inyo domes, California, B. Volcanol., 51, 161–176, 1989."
      },
      { 
        "DOI" : "10.1029/2005GL022491",
        "doi-asserted-by" : "crossref",
        "key" : "ref73",
        "unstructured" : "Takeuchi, S., Nakashima, S., Tomiya, A., and Shinohara, H.: Experimental\nconstraints on the low gas permeability of vesicular magma during decompression, Geophys. Res. Lett., 32, L10312, https://doi.org/10.1029/2005GL022491,\n2005."
      },
      { 
        "DOI" : "10.1016/j.cageo.2015.05.004",
        "doi-asserted-by" : "crossref",
        "key" : "ref74",
        "unstructured" : "Tamburello, G.: Ratiocalc: software for processing data from multicomponent\nvolcanic gas analyzers, Computat. Geosci., 82, 63–67,\nhttps://doi.org/10.1016/j.cageo.2015.05.004, 2015."
      },
      { 
        "DOI" : "10.1007/s004450050222",
        "doi-asserted-by" : "crossref",
        "key" : "ref75",
        "unstructured" : "Tanguy, J.-C., Ribiere, C., Scarth, A., and Tjetjep, W. S.: Victims from\nvolcanic eruptions: a revised database, B. Volcanol., 60, 137–144, 1998."
      },
      { 
        "DOI" : "10.1029/2008JB005742",
        "doi-asserted-by" : "crossref",
        "key" : "ref76",
        "unstructured" : "Thiéry, R. and Mercury, L.: Explosive properties of water in volcanic and\nhydrothermal systems, J. Geophys. Res., 114, B05205,\nhttps://doi.org/10.1029/2008JB005742, 2009."
      },
      { 
        "key" : "ref77",
        "unstructured" : "Van Padang, N.: History of the volcanology in the former Netherlands East Indies, Scripta Geol., 71, 1–76, 1983."
      },
      { 
        "DOI" : "10.1130/G25747A.1",
        "doi-asserted-by" : "crossref",
        "key" : "ref78",
        "unstructured" : "Wadge, G., Ryan, G., and Calder, E. S.: Clastic and core lava components of a\nsilicic lava dome, Geology, 37, 551–554, 2009."
      },
      { 
        "DOI" : "10.1144/GSL.MEM.2002.021.01.06",
        "doi-asserted-by" : "crossref",
        "key" : "ref79",
        "unstructured" : "Watts, R. B., Herd, R. A., Sparks, R. S. J., and Young, S. R.: Growth pattens\nand emplacement of the andesite lava dome at Soufrière Hills Volcano,\nMontserrat, in: The Eruption of Soufrière Hills Volcano, Montserrat, from 1995 to 1999, edited by: Dritt, T. H. and Kokelaar, B. P., Geological\nSociety, Memoirs, London, 21, 115–112, 2002."
      },
      { 
        "key" : "ref80",
        "unstructured" : "Wiart, P.: Impact et gestion des risques volcaniques au Vanuatu, Notes\nTechniques Sciences de la Terre, Geologie-Geophysique, 13,\n1–83, available at: https://www.documentation.ird.fr/hor/fdi:43111 (last access: 5 August 2020), 1995."
      },
      { 
        "key" : "ref81",
        "unstructured" : "Wichmann, A.: Über den Ausbrunch des Gunung Awu am 7. Juni 1892, German\nJ. Geol., 3, 543–546, 1893."
      },
      { 
        "DOI" : "10.1016/j.epsl.2010.03.027",
        "doi-asserted-by" : "crossref",
        "key" : "ref82",
        "unstructured" : "Williamson, B. J., Di Muro, A., Horwell, C. J., Spieler, O., and Llewellin, E. W.: Injection of vesicular magma into an andesitic dome at the\neffusive–explosive transition, Earth Planet. Sc. Lett., 295, 83–90, 2010."
      },
      { 
        "DOI" : "10.1016/j.jvolgeores.2005.04.017",
        "doi-asserted-by" : "crossref",
        "key" : "ref83",
        "unstructured" : "Witham, C. S.: Volcanic disaters and incidents: A new database, J. Volcanol.\nGeoth. Res., 148, 191–233, 2005."
      },
      { 
        "DOI" : "10.1007/BF01081754",
        "doi-asserted-by" : "crossref",
        "key" : "ref84",
        "unstructured" : "Wohletz, K.: Explosive magma-water interactions: Thermodynamics, explosion\nmechanisms, and field studies, B. Volcanol., 48, 245–264, 1986."
      },
      { 
        "DOI" : "10.1016/S0377-0273(01)00280-3",
        "doi-asserted-by" : "crossref",
        "key" : "ref85",
        "unstructured" : "Wohletz, K.: Water/magma interaction: Some theory and experiments on peperite formation, J. Volcanol. Geoth. Res., 114, 19–35, 2002."
      },
      { 
        "DOI" : "10.1002/2017JB013991",
        "doi-asserted-by" : "crossref",
        "key" : "ref86",
        "unstructured" : "Zhang, Q., Guo, F., Zhao, L., and Wu, Y.: Geodynamics of divergent double\nsubduction: 3-D numerical modeling of a Cenozoic example in the Molucca Sea\nregion, Indonesia, J. Geophys. Res.-Solid, 122, 3977–3998,\nhttps://doi.org/10.1002/2017JB013991, 2017."
      },
      { 
        "DOI" : "10.1029/94GL02481",
        "doi-asserted-by" : "crossref",
        "key" : "ref87",
        "unstructured" : "Zielinski, G. A., Fiacco, R. J., Whitlow, S., Twickler, M. S., Germani, M.\nS., Endo, K., and Yasui, M.: Climatic impact of the AD&amp;thinsp;1783 eruption of Asama (Japan) was minimal: evidence from the GISP2 ice core, Geophys. Res.\nLett., 21, 2365–2368, 1994."
      },
      { 
        "DOI" : "10.1016/0029-5493(94)00880-8",
        "doi-asserted-by" : "crossref",
        "key" : "ref88",
        "unstructured" : "Zimanowski, B., Frohlich, G., and Lorenz, V.: Experiments on steam explosion\nby interaction of water with silicate melts, Nucl. Eng. Des., 155, 335–343, 1995."
      }
    ],
  "reference-count" : 88,
  "references-count" : 88,
  "relation" : { 
      "cites" : [  ],
      "has-preprint" : [ { 
            "asserted-by" : "subject",
            "id" : "10.5194/nhess-2020-27",
            "id-type" : "doi"
          } ]
    },
  "score" : 1.0,
  "short-title" : [  ],
  "source" : "Crossref",
  "subject" : [ "General Earth and Planetary Sciences" ],
  "subtitle" : [  ],
  "title" : "Insights into the recurrent energetic eruptions that  drive Awu, among the deadliest volcanoes on Earth",
  "type" : "article-journal",
  "URL" : "http://dx.doi.org/10.5194/nhess-20-2119-2020",
  "volume" : "20"
}
