"""Assorted Exceptions."""


class ConfigError(Exception):
    """Configuration error."""
